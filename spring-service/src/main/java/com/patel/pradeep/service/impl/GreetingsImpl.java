package com.patel.pradeep.service.impl;

import org.springframework.stereotype.Service;

import com.patel.pradeep.service.Greetings;

@Service
public class GreetingsImpl implements Greetings {
	
	@Override
	public String hello(String name) {
		return "Hello, " + name + " !!!";
	}
	
}
